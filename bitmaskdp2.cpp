#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<60

typedef long long ll;
typedef long double ld;

using namespace std;
ll d[20];
vector<vector<ll> >dis;
vector<ll>dp;
 ll n , m ,ans =0 ;
 bool vis[20];
vector<ll>vec;

ll dfs(ll nd)
{
    vis[nd]=true;
    Rep(i , n) if(dis[nd][i] != inf && !vis[i])   dfs(i);
    return 0;
}
ll F(ll mask)
{
    static ll allmask = (1 <<vec.size())-1;
    if(mask == allmask) return 0;
    if(dp[mask] != inf && mask != 0)
        return dp[mask];
    Rep(i, vec.size())
        Rep(j, vec.size())
            if(i != j && !(mask & (1<<i | 1<<j)))
                dp[mask] = min(dp[mask] , dis[vec[i]][vec[j]] + F(mask | (1<<i | 1<<j)));
    return dp[mask];
}
int main()
{

    Set(d, 0);dis.assign(20 , vector<ll>(20  , inf));
 //   Test;
    cin>>n>>m;
    dp.assign((1<<n) , inf);
    Rep(i , m)
    {
        ll x, y ,w;
        cin>>x>>y >>w;x-- ,y--;
        d[x]++,d[y]++; ans+=w;
        dis[x][y]= min(dis[x][y] ,w );
        dis[y][x]=dis[x][y];
    }
    dfs(0);
    Rep(i , n)
        if(!vis[i]&& d[i])
            return cout<<"-1"<<endl , 0;
    Rep(k , n)
    {
        Rep(i ,n)
        {
            Rep(j ,n)
                if(dis[i][j] > dis[i][k] + dis[k][j] && dis[i][k] != inf && dis[k][j] != inf)
                    dis[i][j] = dis[i][k] + dis[k][j];
        }
    }
    Rep(i , n)
        if(d[i] & 1)
            vec.push_back(i);
    return cout<<F(0)+ans <<endl , 0;
}